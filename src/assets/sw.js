const CACHE_NAME = "libros-facilito-v1";
const cache_urls = [
	"/",
	"/home",
	"/about",
	"/index.js",
	"/assets/style.css",
	"/assets/indigo-pink.css"
];

self.addEventListener("install",function(ev){
	
	caches.open(CACHE_NAME)
				.then(function(cache){
					console.log("Cache opened")
					return cache.addAll(cache_urls)
				})
})

self.addEventListener("activate",function(ev){
	ev.waitUntil(
		caches.keys().then(function(cache_names){
			return Promise.all(
				cache_names.map(function(cache_name){
					if(CACHE_NAME !== cache_name){
						return caches.delete(cache_name)
					}
				})
			)
		})
	)
})

self.addEventListener("fetch",function(ev){
	
	ev.respondWith(
		caches.match(ev.request)
					.then(function(response){
						if(response){
							return returnFromCacheOrFetch(ev.request) //Devolviendo del cache
						}
						return fetch(ev.request)
					}).catch(function(err){
						
						if(ev.request.mode == "navigate"){
							return caches.match("/home")
						}
					})
	)
});

self.addEventListener("push",function(ev){
	if(ev.data){
		console.log(ev.data.json());
		let notification = ev.data.json().notification;
		self.registration.showNotification(notification.title,{
			icon: '/assets/logo.png',
			body: notification.body
		})
	}else{
		console.log("No data");
	}
});

self.addEventListener('notificationclick', function(ev) {
    // Close notification.

    console.log(ev);
    ev.notification.close();

    const foundPromise = clients.matchAll().then((clients) =>{
    	console.log(clients);
    	return clients.filter(c =>{
    		const path = /^.*?:\/\/.*?(\/.*)$/.exec(c.url)[1];
    		return path == '/';
    	});
    });

    const openPromise = foundPromise.then(activeClients => {
    	if(activeClients.length > 0){
    		// Was fond
    		return Promise.resolve(activeClients[0].focus());
    	}

    	return clients.openWindow('/').catch(err => console.log(err));
    })

    ev.waitUntil(openPromise);

});



// http://12devsofxmas.co.uk/2016/01/day-9-service-worker-santas-little-performance-helper/
function returnFromCacheOrFetch(request) {
  const cachePromise = caches.open(CACHE_NAME);
  const matchPromise = cachePromise.then(function(cache) {
    return cache.match(request);
  });

  return Promise.all([cachePromise, matchPromise]).then(function([cache, cacheResponse]) {
    // Kick off the update request
    const fetchPromise = fetch(request).then(function(fetchResponse) {
      // Cache the updated file and then return the response
      cache.put(request, fetchResponse.clone());
      return fetchResponse;
    })
    // return the cached response if we have it, otherwise the result of the fetch.
    return cacheResponse || fetchPromise;
  });
}


