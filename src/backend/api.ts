import * as express from 'express';
import {Logro} from './models/logro';
import {Subscription} from './models/subscription';

let ObjectId = require('mongodb').ObjectID;
const router = express.Router();


router.post('/subscriptions',(req,res)=>{
  console.log(req.body);
  Subscription.findOrCreate({endpoint: req.body.endpoint},req.body,
                function(err,subscription){
                  if(err)
                    return res.json({error: "Unable to save subscription" });
                  res.json(subscription);
                });
        
});

// router.post('/subscriptions/remove',(req,res)=>{
//   const endpoint = req.body.endpoint;

//   Subscription.remove({endpoint: endpoint})
//           .then(result => res.json({data: {success: true}}))
//           .catch(err => res.json({error:{ message: "Could not remove"}}));
        
// });
router.get('/demo',(req,res)=>{
  Subscription.find({}).then(subscriptions=>{
    res.json(subscriptions);
  })
});

router.post('/demo',(req,res)=>{
  Subscription.sendNotifications(req.body).then(result=>{
    res.json(result);
  })  

  // console.log(data);

  // let sendNotificationsPromise = Subscription.find({})
  //   .then(subscriptions => {
  //     // let promiseChain = Promise.resolve();
  //     // subscriptions.forEach(subscription => {
  //     //   promiseChain.then(() => {
  //     //     return subscription.sendNotification(data)
  //     //   });
  //     // });
  //     console.log(subscriptions[0]);
  //     subscriptions[0].sendNotification(data).then(response => {
  //       console.log(response);
  //       res.json({})
  //     }).catch(res.json);

  //     // promiseChain.then(()=> res.json({data: {success: true}}))
  //     //             .catch(err => res.json({error: "Could not send subscriptions" }));
  //   })
});

router.route("/:id")
  .get((req,res)=>{
    Logro.findById(req.params.id).then((doc)=> res.json(doc));
  })
  .put((req,res)=>{
    
    let newData = {};
    if(req.body.title) newData["title"] = req.body.title;
    if(req.body.description) newData["description"] = req.body.description;
    if(req.body.author) newData["author"] = req.body.author;
    // new => true es necesario para obtener la última versión
    Logro.findByIdAndUpdate(req.params.id,newData,{new:true})
         .then((doc) => res.json(doc));
        
  })
  .delete((req,res)=>{
    Logro.findByIdAndRemove(req.params.id)
         .then(()=> res.json({message:"Successfully deleted"}));
  })

router.route("/")
    .get((req,res)=>{
      let findQuery;
      if(req.query.from && req.query.from.length > 0){
        let objectID = new ObjectId(req.query.from);
        findQuery = Logro.find({'_id': {$gt: objectID}});
      }else if(req.query.since && req.query.since.length > 0){
        let objectID = new ObjectId(req.query.since);
        console.log(objectID);
        findQuery = Logro.find({'_id': {$lt: objectID}})
                          .limit(10);
      }else{
        findQuery = Logro.find({})
      }
      findQuery.sort({'_id':'desc'}).exec().then((doc) => res.json(doc));
    })
    .post((req,res)=>{
      console.log(req.body);

      const logro = new Logro({
        title:req.body.title,
        description: req.body.description,
        author: req.body.author
      });

      const logroSavedPromise = logro.save();

      const sendSubscriptionPromises = logroSavedPromise.then(logro => {
        return Subscription.sendNotifications(logro.notificationPayload());
      });

      Promise.all([logroSavedPromise,sendSubscriptionPromises])
             .then(([logro, subscriptionResult]) => res.json(logro) )
             .catch(err=>{
                res.json({error: {message: "Something went wrong"}});
              });
    });

export const RouterApi = router;