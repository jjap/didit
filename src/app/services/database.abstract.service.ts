import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import { Logro } from '../logros/logro';

export interface DBService {
	database : Dexie;
	get();
	sync(logros : Logro[]);
}