import { Injectable } from '@angular/core';
import { NotificationService} from './notifications.abstract.service';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { LogrosUrl  } from '../logros.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
declare var Notification;
declare var Promise;
@Injectable()
export class NotificationServiceBrowser implements NotificationService{

	constructor(private http : Http){
	}

	requestPermission(){
		return new Promise((resolve,reject)=>{
			if(!('requestPushNotifications' in window)) reject("Could not complete");

			(window as any).requestPushNotifications()
							.then(pushSubscription => {

								console.log(JSON.stringify(pushSubscription));

								this.saveSubscription(JSON.stringify(pushSubscription)).subscribe(data =>{
									console.log(data);
									resolve(data);
								})

							}).catch(reject);	
		})
		

	}
	saveSubscription(data){
		let dataJSON = JSON.parse(data);
		return this.http.post(LogrosUrl+"/subscriptions",dataJSON)
						.map(response => response.json())
						.catch(err=> Observable.throw("Algo salio mal"));
	}

	removeSubscription(endpoint){
		return this.http.post(LogrosUrl+"/subscriptions/remove",{
			endpoint: endpoint
		}).map(response => response.json())
			.catch(err=> Observable.throw(JSON.stringify(err)));
						
	}

	isPermitted() : Promise<any>{
		if(!('hasPermission' in window)) return Promise.resolve(false);

		return (window as any).hasPermission();
		// return Notification.permission === "granted";
	}

	togglePermission(){
		let hasPermissionPr = this.isPermitted();


		return hasPermissionPr.then(permission => {
			if(permission)
				return this.removePermission();
			return this.requestPermission();
		})
	}

	removePermission(){
		return new Promise((resolve,reject)=>{
			if(!('unsubscribe' in window)) reject("Could not complete");
			(window as any).unsubscribe()
					.then(([pushSubscription,result])=>{
						console.log(pushSubscription);
						resolve(result);
						// console.log(pushSubscription.endpoint);
						// this.removeSubscription(pushSubscription.endpoint)
						// 		.subscribe(resolve);
					}).catch(reject);	
		})
		
	}

}