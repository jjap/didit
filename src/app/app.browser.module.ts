import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UniversalModule, isBrowser, isNode } from 'angular2-universal/browser'; // for AoT we need to manually split universal packages
import { Http } from '@angular/http'
import { HomeModule } from './home/home.module';
import { LogrosModule } from './logros/logros.module';
import { AboutModule } from './about/about.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Cache } from './universal-cache';
import { MaterialModule } from '@angular/material';

import {AddingComponent} from './adding/adding.component';

import {LogrosService} from './services/logros.service';
import { DBServiceBrowser } from './services/database.browser.service';
import { NotificationServiceBrowser } from './services/notifications/notifications.browser.service';


import {ShellNoRender,ShellRender} from '@angular/app-shell/app/shell';
import {IS_PRERENDER} from '@angular/app-shell/app/prerender';

export function getDatabase(){
  return new DBServiceBrowser();
}

export function getNotificationService(http : Http){
  return new NotificationServiceBrowser(http);
}

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ShellRender,
    ShellNoRender,
    AddingComponent
  ],
  imports: [
    MaterialModule.forRoot(),
    UniversalModule, // BrowserModule, HttpModule, and JsonpModule are included
    FormsModule,

    HomeModule,
    AboutModule,
    LogrosModule,

    AppRoutingModule
    
  ],
  providers: [
    { provide: 'isBrowser', useValue: isBrowser },
    { provide: 'isNode', useValue: isNode },
    {provide:IS_PRERENDER,useValue:isNode},
    Cache,
    LogrosService,
    {provide: 'DBService', useFactory: getDatabase},
    {  provide: 'NotificationService', 
      useFactory: getNotificationService,
      deps: [Http]
    }
  ]

})
export class MainModule {
  constructor(public cache: Cache) {
  }
}
