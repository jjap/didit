import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogrosComponent } from './logros.component';
import { LogrosRoutingModule } from './logros-routing.module';

import { MaterialModule } from '@angular/material';

@NgModule({
  imports: [
  	MaterialModule.forRoot(),
    LogrosRoutingModule,
    CommonModule
  ],
  declarations: [
    LogrosComponent
  ]
})
export class LogrosModule { }
